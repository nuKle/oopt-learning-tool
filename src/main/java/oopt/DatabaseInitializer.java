/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt;

import oopt.model.DesignPattern;
import oopt.model.DesignPrinciple;
import oopt.service.DesignPatternRepository;
import oopt.service.DesignPrincipleRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/* Class for populating the database with design pattern content.
 */
@Component
public class DatabaseInitializer implements CommandLineRunner {

    private final DesignPatternRepository designPatternRepository;
    private final DesignPrincipleRepository designPrincipleRepository;

    @Autowired
    public DatabaseInitializer(DesignPatternRepository designPatternRepository,
    		DesignPrincipleRepository designPrincipleRepository) {
    	
        this.designPatternRepository = designPatternRepository;
        this.designPrincipleRepository = designPrincipleRepository;
    }

    // Populates our database with design principles and patterns each time
    // the server starts.
    @Override
    public void run(String... strings) throws Exception {
    	/*
    	designPatternRepository.deleteAll();
    	designPrincipleRepository.deleteAll();
    	
    	List<DesignPrinciple> principles = new ArrayList<>();
    	principles.add(new DesignPrinciple("Single-Responsibility Principle",
    			"Simply put, single-responsibility principle dictates that each"
    			+ " class in a system should have a single responsibility and "
    			+ "purpose."));
    	principles.add(new DesignPrinciple("Open/closed Principle",
    			"The open closed principle states that a class in "
    			+ "object-oriented programming should be closed for "
    			+ "modification but open for extension. The class should be"
    			+ " designed so that you don’t need to change the class every"
    			+ " time a requirement changes. Rather you should design your "
    			+ "classes so that they can be extended so that one doesn’t need"
    			+ " to modify the existing class and will create a new subclass "
    			+ "instead. It will be hard to predict the future: you need to"
    			+ " know what is likely to change and design your classes"
    			+ " accordingly. One might need to do some refactoring to achieve"
    			+ " open/closed principle. Sometimes it might be possible to"
    			+ " recognize a pattern in the requirements changes and plan"
    			+ "accordingly."));
    	principles.add(new DesignPrinciple("Liskov Substitution Principle",
    			"This principle is originally written like this: “Subtype"
    			+ " Requirement: Let f(x) be a property provable about objects"
    			+ " x of type T. Then y(x) should be true for objects y of type"
    			+ " S where S is a subtype of T”. The behavior of all subtypes"
    			+ " should be the same. When two objects are quite alike, but"
    			+ " they still have some differences in behavior, they should"
    			+ " not be subclasses of same parent class. It is better write"
    			+ " separate classes for similar objects in cases like this."));
    	principles.add(new DesignPrinciple("Interface Segregation Principle",
    			"The interface segregation principle states that no client"
    			+ " should be forced to depend on methods it does not use."
    			+ " The main point is to split interfaces that are very large"
    			+ " into smaller and more specific ones so that the clients will"
    			+ " only have to know about the ones that are of interest to"
    			+ " them. So, many client-specific interfaces are better than"
    			+ " one general-purpose interface. The idea is that each"
    			+ " interface is very focused. "));
    	principles.add(new DesignPrinciple("Dependency Inversion Principle",
    			"Dependency inversion principle is all about minimizing coupling"
    			+ " by depending upon abstractions, not concrete classes."
    			+ " High-level modules should not depend on low-level modules"
    			+ " or use them directly. If you had to make changes to something,"
    			+ " it could heavily affect everything else. Instead, both"
    			+ " high-level and low-level modules should depend on abstractions."
    			+ " Traditionally they should have an abstraction layer between"
    			+ " them to avoid such problems."));
    	
    	principles = designPrincipleRepository.save(principles);

    	designPatternRepository.save(new DesignPattern("Singleton Pattern", "Singleton pattern is a " +
				"way of ensuring there’s only one instance of a class at during your program’s execution. Singletons " +
				"are better than having just global variables: they can be instantiated lazily which is an advantage " +
				"if the the object is resource intensive. Singleton pattern is also different from static classes. " +
				"Singleton is a single instance of an object and thus can be passed around as parameter, whereas " +
				"static class is a collection of static methods and thus is not an instance. Singleton pattern is " +
				"implemented by making the class constructor access private and providing a getInstance method for " +
				"getting that single instance of the object. The instance can be created when first time calling " +
				"this getInstance method.", "An example of Singleton pattern could be a student record " +
				"archive. When you want to change your address, you should go to one place and one place only to " +
				"change the address. When somebody comes and asks for your for your address, it can be found and it " +
				"will be up to date. If there were multiple record archives in same school, one might not find your " +
				"record at all, if he/she accidentally went to look from wrong one, or the records might be out of " +
				"sync. This is an example where single point of truth is beneficial.",
				"https://www.youtube.com/embed/b3-uQg3zcmY", Arrays.asList("single_instance"), principles,
				"images/uml_singleton.png"));
    	
    	designPatternRepository.save(new DesignPattern("Command Pattern", "Command pattern " +
				"encapsulates method invocation. It allows the caller (invoker) not to worry about how the command " +
				"will be executed. The command execution will be delegated to a separate implementation of the " +
				"command interface. The Invoker knows only about the Command interface, and the Command " +
				"implementation doesn't know about the Invoker. Using this pattern, undo and logging can be easily " +
				"implemented. For example, undo-operation can be done by remembering previous command that was " +
				"executed.", "An example of Command Pattern could be homework assignment in school. Teacher" +
				" (Invoker) can assign an essay work for students. The assignment may contain some instructions but" +
				" ultimately teacher doesn’t need to know exactly how the assignment was done as long as it was done" +
				" and the teacher can give same assignment regardless of student (concrete command) type. The student" +
				" can then execute the command the way he/she prefers.",
				"https://www.youtube.com/embed/Zcf4aZwm92E", Arrays.asList("decouple", "open-closed_principle"), null,
				"images/uml_command.png"));
    	
    	designPatternRepository.save(new DesignPattern("Adapter Pattern", "Adapter pattern is easily" +
				" understood with the real world example of adapters like HDMI-DisplayPort-adapters. You have two " +
				"pieces that don’t quite fit together and you are not able to modify either one of the pieces, so you" +
				" make a third piece that fits the first two pieces together. Similarly, adapter pattern in object " +
				"oriented design fits together two existing interfaces that don’t quite match with an adapter object " +
				"that acts as a middleman mapping the client requests to be suitable to existing, non-modifiable " +
				"interface.", "Adapter pattern is easily understood with the real world example of adapters" +
				" like HDMI-DisplayPort-adapters. You have two pieces that don’t quite fit together and you are not" +
				" able to modify either one of the pieces, so you make a third piece that fits the first two pieces" +
				" together. ", "https://www.youtube.com/embed/oy0D6rp5nX8", Arrays.asList("legacy_code"), null,
				"images/uml_adapter.png"));

        designPatternRepository.save(new DesignPattern("Observer Pattern", "Observer pattern is a " +
				"software design pattern that keeps your objects in the know when something they might care about " +
				"happens. It is one of the most heavily used patterns used in the JDK (Java Development Kit). It’s " +
				"really useful. In the observer pattern, an object maintains a list of other objects that need to " +
				"know if something happens. The object that maintains the list is called the subject. The objects " +
				"that need to know if something happens are called observers. Observer pattern is typically defined " +
				"like this: “The Observer Pattern defines a one-to-many dependency between objects so that when one " +
				"object changes state, all of its dependents are notified and updated automatically”.", "" +
				"Imagine how magazine subscriptions work. First the publisher begins publishing magazines. Customers" +
				" subscribe to a particular publisher and whenever the publisher publishes a new edition, the new " +
				"edition gets delivered to the customer. As long as the customer remains a subscriber, the new " +
				"magazine gets delivered. This is basically how observer pattern works. In this case the publisher " +
				"is called the subject and the subscribers are called the observers. Publisher maintains the list of " +
				"its subscribers and sends them the new edition when it is out. This applies to your school’s school " +
				"paper too. Whenever a new edition is ready, the students are notified that it is now available for " +
				"them. Now your school is the subject and the students are the observers.",
        		"https://www.youtube.com/embed/IYopMIBUGI0",Arrays.asList("decouple"), null,
				"images/uml_observer.png"));


        designPatternRepository.save(new DesignPattern("Decorator Pattern", "Decorator pattern is a" +
				" way to decorate classes using a form of object composition. Decorating enables the possibility to " +
				"give objects new responsibilities without making any code changes to the underlying classes. The " +
				"decorator pattern attaches additional responsibilities to an object dynamically. Decorator provides " +
				"a flexible alternative to subclassing for extending functionality. ", "For instance, " +
				"imagine a coffee machine at your school. There are multiple condiments offered for your coffee. " +
				"There is milk, sugar and cream, for example. Think each of these as a decorator. You can decorate" +
				" your coffee as you wish. When you choose your coffee and condiment, the coffee gets decorated with " +
				"it and its possible extra cost is added to the price of the coffee. So, the concrete component " +
				"(coffee) gets decorated with the concrete decorator (milk, sugar, cream).",
        		"https://www.youtube.com/embed/K2IAfBauyIQ", Arrays.asList("single-responsibility_principle"), null,
				"images/uml_decorator.png"));
        
        designPatternRepository.save(new DesignPattern("Factory Pattern", "If you are already " +
				"familiar with the Java language, you probably know about the “new” operator. But actually there is " +
				"more to making objects that just that. Let me introduce you the factories. In “real life” factories " +
				"produce products. In the world of programming factories produce objects. The Factory Method Pattern " +
				"defines an interface for creating an object, but lets subclasses decide which class to instantiate. " +
				"So, factories handle the details of object creation. They allow for a lot more long-term " +
				"flexibility. It also allows a more decoupled and more testable design for the code. With factories" +
				" we can also create objects without exposing the creation logic to the client.", "Let’s " +
				"imagine your school as a factory. As a factory, that produces graduates. It is a place that handles" +
				" the details of graduate creation. A freshman goes in and eventually a graduate comes out. Inside" +
				" the school the freshman is molded to have a precise set of knowledge. As a factory in programming" +
				" can produce different kinds of objects, the school can create different kinds of graduates. Some " +
				"have better skills in languages and others exceed in math. ",
        		"https://www.youtube.com/embed/JIaQcISBWjM", Arrays.asList("object_creation", "decouple"), null,
				"images/uml_factory.png"));
        
        designPatternRepository.save(new DesignPattern("Facade Pattern", "The facade pattern can be" +
				" understood as way of unifying subsystem interfaces. Its intent is to make use of a subsystem easier" +
				" for the client; it specifies a single class that holds methods for using a subsystem. The facade " +
				"pattern is connected to the design principle of least knowledge; it allows avoiding tight coupling " +
				"between user code and the system that is being used. A basic UML-diagram of this pattern is shown " +
				"in picture below, where Client is the code using the Facade class and Facade is the class that " +
				"simplifies accessing subsystem classes shown below.", "You might be able to understand this" +
				" pattern more easily by comparing it to the real-world example of a student councilor. Here your " +
				"school’s counselor is the person who answers your question about your studies, be it about which " +
				"courses to take or regarding your next educational step etc. The counselor simplifies this process " +
				"a great deal for you; he or she accesses different sources of information and combines this " +
				"information for you. You can also search for all this information yourself but then you could face " +
				"a lot of unnecessary complexity.",
        		"https://www.youtube.com/embed/B1Y8fcYrz5o", Arrays.asList("interface_simplification"), null,
				"images/uml_facade.png"));
        
        designPatternRepository.save(new DesignPattern("Template Method Pattern", "The template " +
				"method allows its users to specify parts of an algorithm. The method for this algorithm is defined " +
				"in a base class and it calls other methods as algorithm steps. Some of these methods are implemented" +
				" fully in the subclasses.A basic UML-diagram of this pattern is shown in picture below, where the " +
				"concrete class specified implementation for three abstract methods. These methods are the algorithm" +
				" steps for the method specified in the base class. This pattern relates to the Hollywood design " +
				"principle by restricting communication from sub classes and allowing it through base classes to the" +
				" aforementioned sub classes.", "You might understand this design pattern more easily by " +
				"comparing it to the example of a teacher teaching some subject to a group of students. The teacher" +
				" goes through some task consisting of steps; he or she specifies some of these steps but leaves the" +
				" rest for students to do as a homework assignment. The students complete these steps on their own " +
				"and thereby solve the task.","https://www.youtube.com/embed/VdsYx-r2-2Y", Arrays.asList("frameworks"),
				null, "images/uml_template_method.png"));
        
        designPatternRepository.save(new DesignPattern("Iterator Pattern", "The iterator pattern " +
				"allows you to access elements of collections in a similar manner. Specifically, a class that holds" +
				" some collection can supply it as an iterator that allows the client code to iterate over said " +
				"collection. Elements of this collection can be accessed in sequence without any knowledge of the" +
				" collection’s implementation details. Implementing an iterator is one form of encapsulation, and" +
				" therefore iterators can be perceived important for continued maintenance of software products. The" +
				" UML diagram below shows the basic implementation of an iterator; a client accesses some collection" +
				" from a specified class through an iterator, which implements the iterator interface. Here the " +
				"iterator can be accessed polymorphically as an interface type by calling createIterator() method of" +
				" the class.", "One example of an iterator could be iterating over a collection of school " +
				"subjects that a particular student is studying. These subjects could be stored as a list in the " +
				"student class and as some other data structure in the teacher class. A school principal could look " +
				"through these collections with the help of a client side class that uses the iterators supplied by" +
				" student and teacher classes to iterate over subject definitions.",
				"https://www.youtube.com/embed/gGwLgqSmG7U", Arrays.asList("single-responsibility_principle"), null,
				"images/uml_iterator.png"));
        
        designPatternRepository.save(new DesignPattern("Composite Pattern", "The composite pattern " +
				"is used to compose objects into tree structures to represent part-whole hierarchies. A group of " +
				"objects can then be treated in the same way as a single instance of an object, or the same " +
				"operations that could be performed on the primitive objects are also possible with the composites." +
				" Even the most complex tree structures can be easily treated as a single entity. The composite " +
				"pattern makes the code simpler and less error-prone.", "Imagine a teacher teaching a " +
				"classroom full of students. In this example, the students are the single objects or leaves and the" +
				" whole class is the composite of those students. While the teacher could give some task or exercise " +
				"one by one to each student individually, it is much easier and faster give it to the entire class at" +
				" once. Each individual student will still get the task and work on it on their own.",
        		"https://www.youtube.com/embed/fLTg-_ePPCM", Arrays.asList("liskov_substitution_principle"), null,
				"images/uml_composite.png"));
        
        designPatternRepository.save(new DesignPattern("Strategy Pattern", "Strategy pattern allows" +
				" selecting the behavior of a class at runtime. This is useful when classes only differ in their " +
				"behavior. In this case you should isolate the algorithms in separate classes. Strategy pattern " +
				"defines a family of algorithms, encapsulates each one and makes them interchangeable. The algorithm" +
				" varies independently from the client that uses it. Strategy pattern is implemented by creating an" +
				" interface and concrete classes that implement this interface. Finally, a context object handles " +
				"the changes in the behaviors.", "Strategy pattern could be applied to a student’s behavior," +
				" for example. The behavior changes based on the current situation and the subject that is being " +
				"studied at the moment. The student would act differently during a chemistry class or history class." +
				" Other situations such as a lunch break could also have a unique behavior.",
				"https://www.youtube.com/embed/X44RJS1j6lU", Arrays.asList("open-closed_principle", "decouple"), null,
				"images/uml_strategy.png"));
        
        designPatternRepository.save(new DesignPattern("State Pattern", "State pattern implements a" +
				" state machine that allows changing an object’s behavior when its internal state changes. Each state" +
				" is implemented as a derived class of the state pattern interface and the state transitions are " +
				"implemented by invoking the methods defined by the pattern’s superclass. A class behaves basically " +
				"like a different class depending on its state. State pattern can be used to avoid complicated " +
				"if-else conditional statements.", "As a real-life example, think of a school drink vending" +
				" machine. It could have different states, such as “waiting for customer”, “money inserted” and " +
				"“drink sold”. Possible actions by the user could be “insert money”, “eject money”, “select drink” " +
				"and “receive drink”. Coding this example could require several complicated if-else statements. For " +
				"example, when the customer selects a drink by pressing the button, the behavior has to be different," +
				" based on whether the machine is in the “waiting for customer” or “money inserted” state. You don’t " +
				"want to give the drink, if the customer has not inserted any money yet. The state pattern would " +
				"definitely make implementing this example easier.","https://www.youtube.com/embed/MGEx35FjBuo",
				Arrays.asList("open-closed_principle", "decouple"), null, "images/uml_state.png"));
        
        designPatternRepository.save(new DesignPattern("Proxy Pattern", "Proxy Pattern is a class " +
				"functioning as interface to other object. There are three reasons to add proxy. Protection proxy " +
				"make sure that it is allowed to use a class. This class for example check access to certain class." +
				" Remote proxy is used when the local object fetch information from the remote object. The " +
				"implementation of remote object might be at other server. With a complex object it is possible to " +
				"use a Virtual proxy, which prevents using the original class until it is needed. This reduces the " +
				"need for computer resources.", "Example of the protection proxy is opening a door. There" +
				" might be a code lock to secure teachers’ lounge. There is a door class with method open the door. " +
				"The door opening is restricted to the staff. So protection the proxy class will first check whether " +
				"the opener have right to open the door. If it is ok to open the door, open-method in a class door is" +
				" called.", "https://www.youtube.com/embed/cHg5bWW4nUI", Arrays.asList("decouple"),
				null, "images/uml_proxy.png"));
        
        designPatternRepository.save(new DesignPattern("Compound Pattern", "It is common to patterns" +
				" to interact with other patterns and make a whole system. For example the Model–view–controller(MVC)" +
				" use three components combine one system. The model part have state, data and application logic. The" +
				" view is a presentation of the model. The controller converts user inputs to command for the model " +
				"and view. In MVC-system different parts might be implemented with different patterns. The Model " +
				"uses Observer pattern, The View Composite Pattern and The Controller Strategy Pattern.", "",
        		"https://www.youtube.com/embed/dTVVa2gfht8", Arrays.asList("model-view-controller","liskov_substitution_principle",
        				"open-closed_principle", "decouple"), null, ""));
        */
    }
}
