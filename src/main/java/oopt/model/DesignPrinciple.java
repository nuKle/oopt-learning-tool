/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/* Model class for a design principle. Note that getters and setters have been
 * defined in order to allow Spring framework object mapping (database queries).
 */
@Document(collection = "design_principles")
public class DesignPrinciple {
	
	@Id
	private String id;
	
	private String name;
	private String description;
	
	public DesignPrinciple(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
