/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/* Model class for a design pattern. Note that getters and setters have been
 * defined in order to allow Spring framework object mapping (database queries).
 */
@Document(collection = "design_patterns")
public class DesignPattern {
	
    @Id
    private String id;
    @DBRef
    private List<DesignPrinciple> designPrinciples;
    
    private List<String> tags;
    
    private String name;
    private String description;
    private String youtube;
    private String umlPath;
    private String example;

    public DesignPattern() {}

    public DesignPattern(String name, String description, String example, String youtube, List<String> tags,
                         List<DesignPrinciple> designPrinciples, String umlPath) {
    	this.name = name;
        this.description = description;
        this.example = example;
        this.youtube = youtube;
        this.tags = tags;
        this.designPrinciples = designPrinciples;
        this.umlPath = umlPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

	public List<DesignPrinciple> getDesignPrinciples() {
		return designPrinciples;
	}

    public void setDesignPrinciples(List<DesignPrinciple> designPrinciples) {
        this.designPrinciples = designPrinciples;
    }

    public String getUmlPath() {
        return umlPath;
    }

    public void setUmlPath(String umlPath) {
        this.umlPath = umlPath;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }
}
