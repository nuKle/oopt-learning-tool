/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.service;

import oopt.model.DesignPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/* A design pattern service class that handles operations on a design pattern
 * model class. Interacts implicitly with the design pattern repository class;
 * Spring framework handles this dependency through the "@Autowired" annotation.
 */
@Service
public class DesignPatternService {
	
	@Autowired
	private DesignPatternRepository designPatternRepository;
	
	public List<DesignPattern> getPatternNames() {
		return designPatternRepository.getNames();
	}
	
	public List<String> getPatternTags() {
		return designPatternRepository.getTags().flatMap(dp -> dp.getTags().stream()).distinct().collect(Collectors.toList());
	}
	
	public List<DesignPattern> getAllPatterns() {
		List<DesignPattern> patterns = new ArrayList<>();
		designPatternRepository.findAll().forEach(patterns::add);
		return patterns;
	}
	
	public DesignPattern getPatternByName(String name) {
		return designPatternRepository.findByNameIgnoreCase(name);
	}
	
	public List<DesignPattern> findPatternsByTags(List<String> tags) {
		 return designPatternRepository.findByTags(tags);
	}
}
