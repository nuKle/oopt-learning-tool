/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import oopt.model.DesignPrinciple;

/* A design principle repository interface for interacting with a MongoDB database;
 * this allows us to perform design pattern queries. Note that defining interfaces
 * in order to perform database queries is done per Spring framework guidelines.
 */
public interface DesignPrincipleRepository extends MongoRepository<DesignPrinciple, String> {

}
