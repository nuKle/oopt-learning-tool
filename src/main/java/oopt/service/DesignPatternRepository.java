/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.service;

import oopt.model.DesignPattern;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/* A design pattern repository interface for interacting with a MongoDB database;
 * this allows us to perform design pattern queries. Note that defining interfaces
 * in order to perform database queries is done per Spring framework guidelines.
 */
public interface DesignPatternRepository extends MongoRepository<DesignPattern, String> {
	@Query(value = "{}", fields = "{ name: 1 }")
	public List<DesignPattern> getNames();
	
	@Query(value = "{}", fields = "{ tags: 1 }")
	public Stream<DesignPattern> getTags();
	
	public DesignPattern findByNameIgnoreCase(String name);
	public List<DesignPattern> findByNameLikeIgnoreCase(String name);
	
	//public List<DesignPattern> findByTagsTag(String tag);
	@Query("{ tags: { $in: ?0 }}")
	public List<DesignPattern> findByTags(List<String> tags);
}
