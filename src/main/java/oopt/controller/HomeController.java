/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/* Controller class for routing home page html requests.
 */
@Controller
public class HomeController {

	// Returns the default (index) html-page.
    @RequestMapping("/")
    public String getIndex() {
        return "index";
    }
}
