/* CT60A7501_09.01.2017 Object-Oriented Programming Techniques
 * Group project part 11
 * Group A
 */

package oopt.controller;

import oopt.model.DesignPattern;
import oopt.service.DesignPatternService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/* Rest controller class for routing design pattern requests. Returns JSON
 * strings for all defined requests.
 */
@RestController
@RequestMapping("/api/design-patterns")
public class DesignPatternController {
	
	@Autowired
	private DesignPatternService designPatternService;
	
	@RequestMapping(method=RequestMethod.GET, value = "all/tags")
	public List<String> getAllPatternTags() {
		return designPatternService.getPatternTags();
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "all/names")
	public List<DesignPattern> getAllPatternNames() {
		return designPatternService.getPatternNames();
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "all")
	public List<DesignPattern> getAllPatterns() {
		return designPatternService.getAllPatterns();
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "/{name}")
	public DesignPattern getPatternByName(@PathVariable String name) {
		return designPatternService.getPatternByName(name);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/find/tag/{tags}")
	public List<DesignPattern> findPatternsByTags(@PathVariable List<String> tags) {
		return designPatternService.findPatternsByTags(tags);
	}
}
