let welcomeTemplate;
let searchResultTemplate;
let designPatternDescriptionTemplate;

/**
 * Function that is run when the document is ready. It compiles the Handlebars templates (from the index.html)
 * and initializes comboboxes.
 */
$(document).ready(() => {
    Handlebars.registerHelper('json', (context) => JSON.stringify(context));
    var source = $("#welcome-template").html();
    welcomeTemplate = Handlebars.compile(source);
    source = $("#search-result-template").html();
    searchResultTemplate = Handlebars.compile(source);
    source = $("#design-pattern-view-template").html();
    designPatternDescriptionTemplate = Handlebars.compile(source);

    $('#changing-content').html(welcomeTemplate());

    $("#nameSelect").select2();
    $("#tagSelect").select2();
    setComboBoxValues();
});

/**
 * Fetches the design pattern data from API when combobox selection changes
 */
const nameSelectionChanged = () => {
    const designPatternName = $("#nameSelect").val();
    $.getJSON(`/api/design-patterns/${designPatternName}`, result => {
        showDesignPatternView(result);
    });
};

/**
 * Fetches the search results of tag search from API when combobox selection changes
 */
const tagSelectionChanged = () => {
    const selectedTagsArray = $("#tagSelect").val();
    if (selectedTagsArray.length > 0) {
        const commaSeparatedTags = selectedTagsArray.join(",");
        $.getJSON(`/api/design-patterns/find/tag/${commaSeparatedTags}`, result => {
            setSearchResults(result);
        });
    } else {
        setSearchResults([]);
    }
};

/**
 * Sets the content of the search results template
 * @param designPatterns an array of design patterns
 */
const setSearchResults = (designPatterns) => {
    $("#changing-content").html(searchResultTemplate(designPatterns));
};

/**
 * Shows the design pattern description view when search result item is clicked.
 * @param designPattern design pattern object
 */
const onSearchResultItemClick = (designPattern) => {
    showDesignPatternView(designPattern);
};

/**
 * Renders the design pattern description view and initialized popovers
 * @param designPattern design pattern object
 */
const showDesignPatternView = (designPattern) => {
    $("#changing-content").html(designPatternDescriptionTemplate(designPattern));
    initDesignPrinciplePopovers();
};

/**
 * Inits the design principle popovers.
 */
const initDesignPrinciplePopovers = () => {
    let popovers = $(".principle-popover");
    popovers.popover();
    let availableLabelTypes = ["label-default", "label-primary", "label-success", "label-info", "label-warning", "label-danger"];
    popovers.each(function () {
        let nextStyle = availableLabelTypes.shift();
        $(this).addClass(nextStyle);
        availableLabelTypes.push(nextStyle);
    });
};

/**
 * Populates the search combobox values.
 */
const setComboBoxValues = () => {
    const patternNamesField = $('#nameSelect');
    $.get("/api/design-patterns/all/names", result => {
        result.forEach(designPattern => {
            patternNamesField.append($("<option />").val(designPattern.name).text(designPattern.name));
        });
    });
    const tagsField = $('#tagSelect');
    $.get("/api/design-patterns/all/tags", tags => {
        tags.forEach(tag => {
            tagsField.append($("<option />").val(tag).text(tag));
        });
    });
};
